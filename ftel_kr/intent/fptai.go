package intent

import (
	"fmt"
	"strconv"
	"strings"

	log "github.com/Sirupsen/logrus"
	fptai "github.com/fpt-corp/fptai-sdk-go"
)

type FPTAI struct {
	app *fptai.Application
}

func NewFPTAI(config map[string]string) (Engine, error) {
	username := config["username"]
	if username == "" {
		return nil, fmt.Errorf("FPT.AI username must not be empty")
	}
	password := config["password"]
	if password == "" {
		return nil, fmt.Errorf("FPT.AI password must not be empty")
	}
	applicationCode := config["application_code"]
	if applicationCode == "" {
		return nil, fmt.Errorf("FPT.AI application_code must not be empty")
	}
	applicationToken := config["application_token"]
	if applicationToken == "" {
		return nil, fmt.Errorf("FPT.AI application_token must not be empty")
	}

	client, err := fptai.NewClient(username, password)
	if err != nil {
		log.Error("FPT.AI failed to create new client: ", err)
		return nil, err
	}

	var f FPTAI
	f.app = client.GetApp(applicationCode, applicationToken)

	return &f, nil
}

func (f *FPTAI) Detect(text string) string {
	resp, err := f.app.Recognize(strings.ToLower(text))
	if err != nil {
		log.Errorf("FPT.AI failed to recognize intent. Error: %s. Text: %s\n", err.Error(), text)
		return ""
	}
	confidence, err := strconv.ParseFloat(resp.Confidence, 64)
	if err != nil {
		return ""
	}

	if confidence < 0.7 {
		return ""
	}
	
	return resp.Intent
}

func (f *FPTAI) AddUtterance(intent, utterance string) error {
	if err := f.app.AddSampleByLabel(intent, utterance); err != nil {
		log.Errorf("FPT.AI failed to add new utterance for intent. Error: %s. Intent: %s. Utterance: %s\n", err.Error(), intent, utterance)
		return err
	}
	return nil
}

func (f *FPTAI) AddIntentUtterances(ius []IntentUtterance) error {
	intents, err := f.app.Intents()
	if err != nil {
		log.Error("FPTAI failed to get intents: ", err)
		return err
	}

	existingIntents := make(map[string]bool)
	for _, i := range intents {
		existingIntents[i.Label] = true
	}

	var errs []error
	for _, iu := range ius {
		if !existingIntents[iu.Intent] {
			i, err := f.app.CreateIntent(iu.Intent, iu.Intent)
			if err != nil {
				log.Error("FPTAI failed to create new intent: ", err)
				errs = append(errs, err)
				continue
			}
			existingIntents[i.Label] = true
		}

		if err := f.app.AddSampleByLabel(iu.Intent, strings.ToLower(iu.Utterance)); err != nil {
			log.Error("FPT AI failed to add sample by label: ", err)
			return err
		}
	}

	if len(errs) > 0 {
		log.Errorf("Erros:\n")
		for i, e := range errs {
			log.Errorf("%d\t%+v\n", i, e)
		}
	}
	
	return nil
}

func (f *FPTAI) DeleteAllIntents() error {
	intents, err := f.app.Intents()
	if err != nil {
		log.Error("FPT AI failed to get all intents: ", err.Error())
		return err
	}

	for _, intent := range(intents) {
		err := f.app.DeleteIntent(*intent)
		if err != nil {
			log.Errorf("Failed to delete intent: %s %v", err.Error(), intent)
			return err
		}
	}

	log.Info("Deleted success")

	return nil
}

func (f *FPTAI) Train() error {
	return f.app.Train()
}