package dialog

import (
	"github.com/michlabs/fbbot"
)
var ftel *fbbot.Dialog

func New() *fbbot.Dialog {
	d := fbbot.NewDialog()

	var selectbot SelectBot
	var welcome Welcome
	var faq FAQ
	var silence Silence
	var err Error
	var noanswer NoAnswer
	var staffRegister StaffRegister
	var goodbye Goodbye

	d.SetBeginStep(selectbot)
	d.SetEndStep(goodbye)

	d.AddTransition(SelectBotEvent, selectbot)
	d.AddTransition(GoWelcomeEvent, welcome)
	d.AddTransition(GoFAQEvent, faq)
	d.AddTransition(ErrorEvent, err)
	d.AddTransition(NoAnswerEvent, noanswer)
	d.AddTransition(GoSlienceEvent, silence)
	d.AddTransition(GoodbyeEvent, goodbye)
	d.AddTransition(StaffRegisterEvent, staffRegister)

	d.PreHandleMessageHook = PreHandlerMessageHook

	ftel = d
	return d
}