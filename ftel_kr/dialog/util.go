package dialog

import (
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/fpt-corp/qna/db"
	"github.com/fpt-corp/qna/intent"
	"github.com/fpt-corp/util"
	// "github.com/fpt-corp/util/debug"
	"github.com/michlabs/fbbot"
)

func CallStaffs(bot *fbbot.Bot, msg *fbbot.Message) {
	staffs, err := db.GetAllStaffs()
	if err != nil {
		log.Error("Error in call staffs ", err.Error())
	}

	messageToStaff := util.Personalize(T("staff_alert_new_message"), &msg.Sender)

	for _, v :=  range staffs {
		u := fbbot.User{
			ID: v.FbID,
		}
		log.Infof("Send alert message to %s (%s)\n", v.FbID, v.Fullname)
		bot.SendText(u, messageToStaff)
	}
}


func HandleQuestion(bot *fbbot.Bot, msg *fbbot.Message) fbbot.Event {
	question := bot.STMemory.For(msg.Sender.ID).Get("question")
	if question == "" {
		question = msg.Text
	}
	log.Infof("Question = %s", question)

	bot.STMemory.For(msg.Sender.ID).Delete("question")
	i := intent.Detect(question)
	switch i {
	case "":
		log.Debug("There's no matching sentence for: ", msg.Text)
		bot.SendText(msg.Sender, util.Personalize(T("dontunderstand"), &msg.Sender))
		return NoAnswerEvent
	case "goodbye":
		return GoodbyeEvent
	default:
		a := db.GetAnswerFor(i)
		if a == "" {
			bot.SendText(msg.Sender, util.Personalize(T("no_answer"), &msg.Sender))
			return NoAnswerEvent
		}

		if strings.Contains(a, "@call_staff") {
			a = strings.Replace(a, "@call_staff", "", -1)
			CallStaffs(bot, msg)
		}

		var arr []string
		a = strings.TrimSpace(a)
		arr = strings.SplitN(a, "\n", -1)
		for _, v := range arr {
			bot.TypingOn(msg.Sender)
			util.SendTextWithImages(bot, msg.Sender, util.Personalize(v, &msg.Sender))
		}
	}
	return StayEvent
}